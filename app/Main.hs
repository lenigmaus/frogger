{-# LANGUAGE FlexibleContexts #-}
{-# OPTIONS_GHC -Wno-incomplete-patterns #-}

module Main where

import Control.Concurrent
    ( newChan, readChan, writeChan, forkIO, Chan )
import Control.Monad.Except ( join, forever )
import Control.Monad.State
    ( join, forever, MonadState(get, put), StateT(runStateT) )
import Control.Monad.Reader
    ( join, forever, MonadReader(ask), ReaderT(runReaderT) )
import System.IO ( hSetEcho, stdin )
import System.Random ()
import System.Console.ANSI ( hideCursor )

import Data
    ( Game(frog, lanes, score),
      Lane(Lane, direction, speed, cars),
      Direction(..),
      Frog(Frog, position),
      Config(screenSize, finishZone, startZone, tickRate),
      Time,
      Event(..) )
import Init ( mkLanes, initGame )
import Util ( after, io, setNoBuffering )
import Render ( renderNext, renderOver )

keys :: [(Char, Direction)]
keys = [('i', U), ('j', L), ('l', R), ('k', D)]

moveFrog :: MonadState Game m => Maybe Direction -> m ()
moveFrog d = do
    game <- get
    let Frog (y, x) = frog game
    case d of
        Just U -> put ( game { frog = Frog (y - 1, x), lanes = lanes game, score = score game })
        Just R -> put ( game { frog = Frog (y, x + 1), lanes = lanes game, score = score game })
        Just D -> put ( game { frog = Frog (y + 1, x), lanes = lanes game, score = score game })
        Just L -> put ( game { frog = Frog (y, x - 1), lanes = lanes game, score = score game })
        Nothing -> return ()

moveLanes :: (MonadReader Config m, MonadState Game m) => m ()
moveLanes = do
    game <- get
    config <- ask
    let (srow, scol) = screenSize config
    let ls = lanes game
    let newLs = map (\l -> case direction l of
                                R -> Lane { cars = map (\(row, col) -> (row, (col + speed l) `mod` scol)) $ cars l, direction = R, speed = speed l }
                                L -> Lane { cars = map (\(row, col) -> (row, (col - speed l) `mod` scol)) $ cars l, direction = L, speed = speed l }
                                ) ls
    put ( game { frog = frog game, lanes = newLs, score = score game })

castTick :: Chan Event -> Time -> IO ()
castTick chan rate = forever $ do
    after rate $ writeChan chan TickEvent
    writeChan chan $ TickEvent

castKey :: Chan Event -> IO ()
castKey chan = forever $ do
  hSetEcho stdin False
  c <- getChar
  hSetEcho stdin True
  writeChan chan (KeyEvent c)

canPlay :: (MonadReader Config m, MonadState Game m) => m Bool
canPlay = do
    config <- ask
    game <- get
    let (srow, scol) = screenSize config
    let Frog pos@(row, col) = frog game
    let ls = lanes game
    return $ null (
                (join . map (filter (==(row    , col - 2)) . cars) $ ls) ++
                (join . map (filter (==(row    , col - 1)) . cars) $ ls) ++
                (join . map (filter (==(row    , col    )) . cars) $ ls) ++
                (join . map (filter (==(row    , col + 1)) . cars) $ ls) ++
                (join . map (filter (==(row    , col + 2)) . cars) $ ls) ++
                (join . map (filter (==(row + 1, col - 2)) . cars) $ ls) ++
                (join . map (filter (==(row + 1, col - 1)) . cars) $ ls) ++
                (join . map (filter (==(row + 1, col    )) . cars) $ ls) ++
                (join . map (filter (==(row + 1, col + 1)) . cars) $ ls) ++
                (join . map (filter (==(row + 1, col + 2)) . cars) $ ls)
              )

isLevelDone :: (MonadReader Config m, MonadState Game m) => m Bool
isLevelDone = do
    config <- ask
    game <- get
    let Frog pos@(frow, _) = frog game
    return $ frow <= finishZone config


loadNextLevel :: (MonadReader Config m, MonadState Game m) => m ()
loadNextLevel = do
    config <- ask
    game <- get
    let (srow, scol) = screenSize config
    let finishingOffset = finishZone config
    let startingOffset = startZone config
    let distance = srow - finishingOffset - startingOffset
    put (game {
        frog    = Frog { position = (srow - startingOffset, scol `div` 2) },
        lanes   = mkLanes ((length . lanes $ game) + 1) finishingOffset distance scol,
        score   = score game + 1})

play :: Chan Event -> ReaderT Config (StateT Game IO) ()
play chan = forever $ do
    event <- io $ readChan chan
    game <- get
    ok <- canPlay
    finished <- isLevelDone
    case (ok, finished, event) of
        (False, _, _) -> renderOver
        (True, False, TickEvent) -> do moveLanes; renderNext
        (True, False, KeyEvent k) -> do moveFrog $ lookup k keys; renderNext
        (True, True, _) -> loadNextLevel

main :: IO ((), Game)
main = do
    setNoBuffering
    hideCursor
    chan <- newChan
    (config, game) <- initGame
    forkIO $ castTick chan (tickRate config)
    forkIO $ castKey chan
    runStateT (runReaderT (play chan) config) game