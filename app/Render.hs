{-# LANGUAGE FlexibleContexts #-}
{-# OPTIONS_GHC -Wno-incomplete-patterns #-}

module Render (
    renderNext,
    renderOver
) where

import System.Console.ANSI ( clearScreen, setCursorPosition )
import Control.Monad.IO.Class ( MonadIO )
import Control.Monad.Reader ( forever, MonadReader(ask) )
import Control.Monad.State ( MonadState(get) )

import Data
    ( Game(frog, lanes, score),
      Lane(Lane),
      Direction(L, R),
      Frog(position),
      Point,
      Config(finishZone, screenSize, blinkRate) )
import Util ( after, io )

renderCharacter :: Char -> Point -> IO ()
renderCharacter c (y, x) = do
    setCursorPosition y x
    putChar c
    return ()

renderFrog :: Frog -> IO ()
renderFrog frog = do
    let p@(row, col) = position frog
    setCursorPosition row (col - 2)
    putStr "\\•^•/"
    setCursorPosition (row + 1) (col - 2)
    putStr "_\\◯/_"
    return ()


renderLane :: Lane -> IO ()
renderLane (Lane cars R _) = mapM_ (renderCharacter '⬜') cars
renderLane (Lane cars L _) = mapM_ (renderCharacter '⬜') cars

renderFinishZone :: (MonadIO m, MonadReader Config m) => Int -> m ()
renderFinishZone zone = do
    config <- ask
    let (_, scol) = screenSize config
    io $ setCursorPosition zone 0
    io $ putStr $ take scol $ repeat '-'
    io $ setCursorPosition (zone `div` 2) (scol `div` 2 - 8)
    io $ putStr "Try to get here"
    return ()

renderNext :: (MonadIO m, MonadReader Config m, MonadState Game m) => m ()
renderNext = do
    config <- ask
    game <- get
    let (srow, scol) = screenSize config
    io clearScreen
    renderFinishZone $ finishZone config
    io $ renderFrog $ frog game
    io $ mapM_ renderLane $ lanes game
    io $ setCursorPosition (srow - 2) (scol - 10)
    io . putStrLn $ "Score: " ++ (show . score $ game)

renderOver :: (MonadIO m, MonadReader Config m, MonadState Game m) => m ()
renderOver = do
    game <- get
    config <- ask
    let (srow, scol) = screenSize config
    io clearScreen
    forever $ do
        after (blinkRate config) $ do
            io $ setCursorPosition (div srow 2) (div scol 2 - 7)
            io $ putStr "Game Over"
            io $ setCursorPosition (div srow 2 + 1) (div scol 2 - 7)
            io $ putStr $ "Score: " ++ (show . score $ game)
        after (blinkRate config) $ io clearScreen