{-# LANGUAGE FlexibleContexts #-}

module Init (initConfig, initGame, mkLanes) where

import Control.Monad.Reader ( MonadIO )
import System.Console.ANSI ( getTerminalSize )
import System.Random ( uniformR, mkStdGen )
import Data.Maybe ( fromJust )
import Data.List ( unfoldr )

import Data
    ( Game(..),
      Lane(..),
      Direction(L, R),
      Frog(Frog, position),
      Point,
      Config(..) )
import Util ( io )

mkCars :: Int -> Int -> Int -> Int -> [Point]
mkCars order length density r =
    map (\(x, y) -> (order, x)) $ filter (\(_ , y) -> y == (density :: Int)) $ zip [0 .. length-1 ] $ take length . unfoldr (Just . uniformR (0, density)) $ mkStdGen r

mkLanes :: Int -> Int -> Int -> Int -> [Lane]
mkLanes count offset distance laneLength =
    take count $ unfoldr (\(order, gen) ->
        Just (
            Lane {
                cars = mkCars (order * (distance `div` (count + 1)) + offset ) laneLength (fst $ uniformR (4 :: Int, 8) gen) (fst $ uniformR (0 :: Int, 10000) gen),
                direction = if (fst . uniformR (0 :: Int, 1) $ gen) == 0 then R else L,
                speed = fst . uniformR (1 :: Int, 3) $ gen
            },
            (order + 1, snd $ uniformR (0 :: Int, 8) gen)))
        (1, mkStdGen 100)

initConfig :: MonadIO m => m Config
initConfig = do
    s <- io getTerminalSize
    return $ Config {
        screenSize  = fromJust s,
        tickRate    = 300,
        blinkRate   = 500,
        finishZone  = 5,
        startZone   = 5
    }

initGame :: MonadIO m => m (Config, Game)
initGame = do
    config <- initConfig
    let (srow, scol) = screenSize config
    let finishingOffset = finishZone config
    let startingOffset = startZone config
    let distance = srow - finishingOffset - startingOffset
    let game = Game {
        frog    = Frog { position = (srow - startingOffset, scol `div` 2) },
        lanes   = mkLanes 1 finishingOffset distance scol,
        score   = 0
    }
    return (config, game)


    