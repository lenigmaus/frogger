module Data (
    Config (..),
    Event (..),
    Point,
    Frog (..),
    Direction (..),
    Lane (..),
    Time,
    Size,
    Game (..)
) where

data Event
    = TickEvent
    | KeyEvent Char
    deriving Show

type Size       = (Int, Int)
type Time       = Int
data Config     = Config {
    screenSize  :: Size,
    tickRate    :: Time,
    blinkRate   :: Time,
    finishZone  :: Int,
    startZone   :: Int
}

type Point      = (Int, Int)
newtype Frog    = Frog { position :: Point } deriving Show
data Direction  = U | R | D | L deriving Show
data Lane       = Lane {
    cars      :: [Point],
    direction :: Direction,
    speed     :: Int
} deriving Show

data Game       = Game {
    frog        :: Frog,
    lanes       :: [Lane],
    score       :: Int
}


