module Util (
  after,
  io,
  setNoBuffering
) where

import Control.Concurrent ( threadDelay )
import Control.Monad.IO.Class ( MonadIO(..) )
import System.IO
    ( hSetBuffering, stdin, stdout, BufferMode(NoBuffering) )

after :: MonadIO m => Int -> m a -> m a
after t m = do io (threadDelay (t * 1000)); m

io :: MonadIO m => IO a -> m a
io = liftIO

setNoBuffering :: IO ()
setNoBuffering = do
  hSetBuffering stdin NoBuffering
  hSetBuffering stdout NoBuffering
