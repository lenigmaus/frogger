# Revision history for frogger

## 0.1.0.0 -- 2022-07-01

* First enjoyable version. Score calculation, nice shapes, some blinking.
